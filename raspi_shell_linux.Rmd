---
title: "Raspberry Pi, Linux und die Shell"
author: "Friedrike Preu"
date: "11 Nov 2017"
output: 
  ioslides_presentation:
    slide_level: 2
    logo: logo.png
    widescreen: true
    css: styles.css
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
library(emo)
```

## Inhalt
- Raspberry Pi
- Arbeiten mit dem Raspberry Pi
- Shell
- Hands On 
- (Linux)

## Weiterführende Ressourcen 
- [https://hackmd.io/BwMwxgzATA7ApgVgLQ2AIxkgLCAJgQyXwDY0Ik0F84AGKKYARntyA===?both#]()

# Der Raspberry Pi 

## Was ist der Raspberry Pi?
### Idee
- Computer zum Experimentieren + Herumspielen
- kleiner Einplatinencomputer mit geringen Anschaffungskosten (< 50 Euro zum Einstieg)
- siehe Historie in [Wikipedia](https://de.wikipedia.org/wiki/Raspberry_Pi#Idee)

### Eigenschaften
- Linux-Betriebssystem
- verschiedene Modelle (siehe [Wikipedia](https://de.wikipedia.org/wiki/Raspberry_Pi#Hardware))
- frei programmierbare Schnittstelle (GPIO)
- neuere Modelle i.d.R.: Wifi, Bluetooth, USB, HDMI

## Verwendung
### Generell
- Mediacenter
- Webserver/Hosting (Website, Blog...)
- Kalender-/Kontakteserver
- IOT Device/Physical Computing (Wetterstation, LEDs,...)
- ... (siehe YT, google, etc.)

### Eigene Projekte/Ideen
- CorrelAid-Datensammlung 
- Text-To-Speech (TTS) Device mit Scanner
- automatischer Podcast Download auf USB Stick

## CorrelAid in Zahlen

![https://correlaid.org/de/network](pictures/correlaid_in_zahlen.png)

## CorrelAid in Zahlen 
### Daten
- Twitter
- Facebook
- Mailchimp 

### Problem
- "scattered" Daten von verschiedenen APIs
- jede Woche aktuelle Daten als JSON auf [Website](https://correlaid.org/media/data/all_weekly.json)

## Mögliche Lösungen
### Derzeitige Lösung 
- (headless) Raspberry Pi: lädt täglich neue Daten + wöchentliches Hochladen
- (vorläufig) [Mlab](mlab.com): tägliches MongoDB backup  
- [GitHub](https://github.com/CorrelAid/correlaid-utils/tree/master/correlaid-analytics): "Synchronisierung" Entwicklung und Raspberry Pi 
- manchmal `r emo::ji("shrug")` und  `r emo::ji("shit")`, dafür `r emo::ji("nerd_face")` x `r emo::ji("100")`

### Alternative Lösung
- Amazon Web Services (AWS): EC2 (Server) + S3 (Storage) (o.ä.)
- `r emo::ji("watch")`, aber `r emo::ji("money_with_wings")` und `r emo::ji("tophat")`

# Arbeiten mit dem Raspberry Pi
## Desktopumgebung PIXEL
- Desktopumgebung ([PIXEL](https://www.raspberrypi.org/blog/introducing-pixel/))

### Vorteile
- normales Arbeiten wie am eigenen Laptop/Desktop

### Nachteile: 
- Hardware (Monitor, Maus, Tastatur)
- Speed
- einige Programme nicht verfügbar (z.B. RStudio)

## VNCServer
- VNC (**V**irtual **N**etwork **C**omputing): "graphical desktop sharing system"
- remote control of the desktop interface

### Vorteile
- siehe oben
- Arbeit mit eigenen Eingabegeräten

### Nachteile
- Speed 
- z.T. Auflösung 

## "Headless"/SSH
- Login von eigenem Computer in die *Shell* mithilfe von SSH (**S**ecure **Sh**ell)
- Shell? 

## 

<iframe src="https://giphy.com/embed/IbYGMVhugrOxi" width="480" height="259" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/IbYGMVhugrOxi">via GIPHY</a></p>

## "Headless"/SSH
### Vorteile
- `r emo::ji("nerd_face")` x `r emo::ji("100")`
- schneller als mit PIXEL (kein grafischer overhead)
- Lernpotenzial / Skills 
- effizient (?) / [Vergleich](http://www.webminal.org/cast/)

### Nachteile
- Lernkurve
    - `cd`, `ls`, `grep`, `crontab`, `nano`/`vim`... ???
- "Synchronisierung" nötig

# Shell
## Wichtigste Commands 
- `ls`: list files
- `cd <dirname>`: change directory to dirname 
- `cat <file>`: Inhalt der Datei ausgeben
- `nano <file>`: Datei erstellen/bearbeiten
- `cp`/`mv <file> <target>`: copy/move file to target
- `R` / `python`: interaktive R / Python Session (Konsole) 
- `mkdir <name>`: Erstelle einen Ordner

### Help?
- `command --help`, e.g. `ls --help` 
- `man command`, e.g. `man ls`

# Hands-On

## Aufgabe
1. SSHe in den Raspberry Pi mit..
    - guest@192.168.1.100 oder .101
    - password: guest
2. Erstelle einen Ordner mit deinem Vornamen + erster Buchstabe Nachnamen (e.g. `friep`)
3. Schreibe ein Mini-R-Skript (nur base-R, Name `dein_name_script.R`), das einen Text-Output (z.B. `.txt`/`.csv`...) in deinem Home-Ordner erstellt.
4. Schedule das R Skript mithilfe eines `cronjob`s, sodass es jede Minute ausgeführt wird. 
5. Überprüfe, ob es funktioniert, mit Shell Commands (`ls -l`)

-> hilfreiche Links im Hackmd! 


# Linux

## 
![Quelle: https://de.wikipedia.org/wiki/Tux_(Maskottchen)#/media/File:Tux.png](pictures/Tux.png)

## Was ist Linux?
- eigtl. Name eines *Kernels*
- ugs.: Betriebssysteme und Distributionen basierend auf dem Linux-Kernel
    - allgemein: Ubuntu, Fedora, Mint, Debian, Arch Linux, ... ([Liste aller Linux Distributionen](https://de.wikipedia.org/wiki/Liste_von_Linux-Distributionen))
    - speziell für RPi: Raspbian, Pidora, ...
- offene/freie Software und Entwicklung
- Installation von Software/Programmen über *Paketmanager* aus *Repositories* (vgl. CRAN)



